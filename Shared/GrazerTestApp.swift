//
//  GrazerTestApp.swift
//  Shared
//
//  Created by Janis Mozumacs on 31/03/2022.
//

import SwiftUI

@main
struct GrazerTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
